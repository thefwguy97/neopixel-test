// Rotary Dial

// NEOPIXEL BEST PRACTICES for most reliable operation:
// - Add 1000 uF CAPACITOR between NeoPixel strip's + and - connections.
// - MINIMIZE WIRING LENGTH between microcontroller board and first pixel.
// - NeoPixel strip's DATA-IN should pass through a 300-500 OHM RESISTOR.
// - AVOID connecting NeoPixels on a LIVE CIRCUIT. If you must, ALWAYS
//   connect GROUND (-) first, then +, then data.
// - When using a 3.3V microcontroller with a 5V-powered NeoPixel strip,
//   a LOGIC-LEVEL CONVERTER on the data line is STRONGLY RECOMMENDED.
// (Skipping these may work OK on your workbench but can fail in the field)

// TheFwGuy
// The program use a Neopixel strip as feedback about reading a rotary dial

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN    1
#define SW1        0    // Signalling switch
#define SW2        2    // Pulse dial switch

// State machine to read rotary dial
#define RD_START  0
#define RD_IDLE   1
#define RD_COUNT  2
#define RD_CHECK  3
#define RD_END    4


// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 16

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)

char rd_status;       // Rotary dial reading state machine 
char active_number;   // Number being read       
char number[10];       // Array to store read numbers (max 10 digits)


// setup() function -- runs once at startup --------------------------------

void setup() {
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  rd_status = RD_START;

  pinMode(SW1,INPUT);
  pinMode(SW2,INPUT);
  
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(15); // Set BRIGHTNESS to about 1/5 (max = 255)

  // Set input pins for rotary dial
  
}


// loop() function -- runs repeatedly as long as board is on ---------------

void loop() {
  rd_state_machine();
  
}

void rd_state_machine () {
  switch (rd_status) {
    case RD_START:
    default:
      active_number = 0;
      strcpy(number, "-");
      colorWipe(strip.Color(  0, 255,   0), 10); // Green
      rd_status = RD_IDLE;
      break;

    case RD_IDLE:
      if (digitalRead(SW1) == LOW) {
        colorWipe(strip.Color(255,   0,   0), 5); // Red
        // Set timer
        rd_status = RD_COUNT;
      }
      break;  

    case RD_COUNT:
      if (digitalRead(SW1) == LOW) {
        
        if (readSwitch(SW2) ) { 
          active_number++;
          while (readSwitch(SW2));   // Wait the pulse to go down
          colorNumber(strip.Color(  0, 0, 255), 0, active_number);        }
      } else {
        // Number finished
        colorWipe(strip.Color(  0, 0, 0), 0); // turn off display
        colorNumber(strip.Color(  0, 0, 255), 0, active_number);
        rd_status = RD_CHECK;
      }
      break;

      case RD_CHECK:
        // TBD
        delay(1000);
        rd_status = RD_START;
        break;
   }  
}

// The function read a button with some debouncing - return true if the pin goes high.
// returns false otherwise
boolean readSwitch(int pin)
{
   if(digitalRead(pin))
   {
      delay(1);
      if(digitalRead(pin))
      {
         return true;
      }
   }
   return false;
}

// Some functions of our own for creating animated effects -----------------

// Fill strip pixels one after another with a color. Strip is NOT cleared
// first; anything there will be covered pixel by pixel. Pass in color
// (as a single 'packed' 32-bit value, which you can get by calling
// strip.Color(red, green, blue) as shown in the loop() function above),
// and a delay time (in milliseconds) between pixels.
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

// Display a number (from 0 to 8)
void colorNumber(uint32_t color, int wait, char number) {
  for(int i=0; i<number; i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}
